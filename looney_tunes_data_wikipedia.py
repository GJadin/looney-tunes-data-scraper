#  Copyright (C) 2022 Guillaume Jadin
import sys, textwrap, eyed3, argparse
import tools_editor as tools
from os import walk, mkdir, rename
from os.path import join, exists, basename
from shutil import move, rmtree
from datetime import datetime
import wikipedia as wiki
import requests
from bs4 import BeautifulSoup

def txt_month_to_nbr_month(txt_month):
    db = {
        "January": "01",
        "Jan": "01",
        "February": "02",
        "Feb": "02",
        "March": "03",
        "Mar": "03",
        "April": "04",
        "Apr": "04",
        "May": "05",
        "June": "06",
        "Jun": "06",
        "July": "07",
        "Jul": "07",
        "August": "08",
        "Aug": "08",
        "September": "09",
        "Sep": "09",
        "October": "10",
        "Oct": "10",
        "November": "11",
        "Nov": "11",
        "December": "12",
        "Dec": "12"
    }
    return db[txt_month.rstrip(".")]

def get_columns(table):
    ncells = []
    for nrow in table.findAll("tr"):
        ncells = nrow.findAll('td')
    return ncells

def get_rows(table):
    return table.findAll("tr")

def parse_table_header(table):
    return [th.text.rstrip() for th in get_rows(table)[0].find_all('th')]

def parse_table_by_columns(table):
    rows = get_rows(table)
    columns = get_columns(table)
    parsed_table = [[] for i in range(len(columns))]
    for nbr, row in enumerate(rows):
        cells = row.findAll('td')
        if len(cells) > 0:
            for i in range(len(columns)):
                if i == len(columns)-1 and len(cells) < len(columns):
                    parsed_table[i].append(None)
                else:
                    parsed_table[i].append(cells[i].find(text=True))
    return parsed_table

def remove_unused_data(header, parsed_table):
    new_ptable = []
    for keeper in ['#', 'Title', 'Release date']:
        i = header.index(keeper)
        new_ptable.append(parsed_table[i])
    return new_ptable

def run_search_wiki(year):
    data = {}
    urls = {
        "1930": "https://en.wikipedia.org/wiki/Looney_Tunes_and_Merrie_Melodies_filmography_(1929%E2%80%931939)",
        "1940": "https://en.wikipedia.org/wiki/Looney_Tunes_and_Merrie_Melodies_filmography_(1940%E2%80%931949)",
        "1950": "https://en.wikipedia.org/wiki/Looney_Tunes_and_Merrie_Melodies_filmography_(1950%E2%80%931959)",
        "1960": "https://en.wikipedia.org/wiki/Looney_Tunes_and_Merrie_Melodies_filmography_(1960%E2%80%931969)"
    }
    if urls.get(year) is not None:
        response = s.get(urls[year], timeout=10)
        soup = BeautifulSoup(response.content, 'html.parser')
        all_tables = soup.find_all('table', {"class": 'wikitable sortable'})
        for itable in range(len(all_tables)):
            c_header = parse_table_header(all_tables[itable])
            c_table = parse_table_by_columns(all_tables[itable])
            p_data = remove_unused_data(c_header, c_table)
            year = p_data[-1][0].strip("\n").split(", ")[1]
            data[year] = p_data
    return data

path = r"/home/guillaume/Videos/Looney Tunes/"
s = requests.Session()
# print(run_search_wiki("1950"))

all_data = {
    "1930": run_search_wiki("1930"),
    "1940": run_search_wiki("1940"),
    "1950": run_search_wiki("1950"),
    "1960": run_search_wiki("1960"),
}
# all_data["1940"]["1940"][2]
tot_episode = (0, 270, 577, 855, 1000)

curr_path, dirs, files = sorted(walk(path, topdown=False))[0]
for file_filtered in sorted(tools.FileParser.filter_files(curr_path, files, ".mkv")):
    file_year = file_filtered.lstrip("(")[:4]
    season = int(file_year[:-1]) - 192
    curr_db = all_data[file_year[:-1]+"0"][file_year]
    name = file_filtered.split(") ")[-1][:-4]
    i = None
    for idx, elem in enumerate(curr_db[1]):
        if elem.strip("?").lower() == name.lower():
            i = idx
            name = elem
            break
    if i is None:
        print("ERROR: (" + file_year + ") " + name)

    else:
        episode = int(curr_db[0][i].strip("\n")) - tot_episode[season-1]

        date = curr_db[2][i].strip("\n").split(" ")
        date[1] = date[1].strip(",")
        date[0], date[1] = "0"+date[1] if len(date[1]) == 1 else date[1], txt_month_to_nbr_month(date[0])

        nfo = open(join(curr_path, "{0}.nfo".format(file_filtered.split(".mkv")[0])), "w")
        nfo.write("""<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>\n""")
        nfo.write("<episodedetails>\n")
        nfo.write("    <title>{0}</title>\n".format(name))
        nfo.write("    <season>{0}</season>\n".format(season))
        nfo.write("    <episode>{0}</episode>\n".format(episode))
        nfo.write("    <aired>{2}-{1}-{0}</aired>\n".format(date[0], date[1], date[2]))
        nfo.write("</episodedetails>")
        nfo.close()
