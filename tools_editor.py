# -*- coding: utf-8 -*-

#  Copyright (C) 2021 Guillaume Jadin
import sys
from os import listdir
from os.path import isdir, isfile, join

class FileParser:
    """
    Functions which help to manage filename
    """

    @staticmethod
    def filter_files(curr_path, files, wanted_ext_file):
        """
        Keep files in the list "files" which have an extension equal at "wanted_ext_file"

        :param str curr_path: the path where the files are (NOT USED)
        :param list[str] files: a filenames list
        :param str wanted_ext_file: a string with the format ".EXTENSION" (EX: '.mp3', '.mp4', '.nfo', ...)
        :return: a list with all the filenames with the extension "wanted_ext_file".
                 if there is no elements with the extension "wanted_ext_file", return an empty list [].
        :rtype: list[str]
        """
        correct_ext = []
        for file in files:
            filename, curr_ext = FileParser.sep_extension(file)
            if curr_ext == wanted_ext_file:
                correct_ext.append(file)
        return correct_ext

    @staticmethod
    def sep_extension(file):
        """
        Separate the file extension with the filename, with the last dot of the file.

        :param str file: a file (a filename with an extension)
        :return: (filename, extension with the dot)
        :rtype: (str, str)
        """
        last_dot_index = file.rfind(".")
        return file[:last_dot_index], file[last_dot_index:]

class Paths:
    """
    Collection with all paths needed
    :cvar MUSIC_VIDEOS: Path to the music videos library.
    :cvar MUSICS: Path to the musics library.
                  Directories/Files in this path must have one of following structures:
                      {PATH}/{FIRST_LETTER_ARTIST}/{ARTIST}/{ARTIST} - {TITLE}.mp3
                      {PATH}/{ARTIST}/{ARTIST} - {TITLE}.mp3
                  Examples : /home/user/Music/G/Genesis/Genesis - Jesus He Knows Me.mp3
                             /home/user/Music/Genesis/Genesis - Jesus He Knows Me.mp3
    """
    # TODO: Change these paths to match with your system
    MUSIC_VIDEOS = r"/home/guillaume/Videos/Music Videos/"
    MUSICS = r"/home/guillaume/Musics/"
