#  Copyright (C) 2022 Guillaume Jadin
import sys, textwrap, eyed3, argparse
import tools_editor as tools
from os import walk, mkdir, rename
from os.path import join, exists, basename
from shutil import copyfileobj
from datetime import datetime
import wikipedia as wiki
import requests
from bs4 import BeautifulSoup

def parse(file):
    filename = remove_extension(file).split(") ")[-1]
    filename = filename.replace(" ", "_")
    return filename

def remove_extension(file):
    return file[:-4]

path = r"/home/guillaume/Videos/Looney Tunes/Looney Tunes - 1960s/"
template_url = "https://looneytunes.fandom.com/wiki/"
s = requests.Session()

curr_path, dirs, files = sorted(walk(path, topdown=False))[0]
for file_filtered in sorted(tools.FileParser.filter_files(curr_path, files, ".mkv")):
    url = template_url + parse(file_filtered)
    response = s.get(url, timeout=10)
    if response.status_code == 200:
        soup = BeautifulSoup(response.content, 'html.parser')
        image_url = soup.find_all('meta', {"property": 'og:image'})
        image_url = image_url[0] if len(image_url) < 2 else image_url[1]
        image_url = image_url.get("content")

        response_image = requests.get(image_url, stream=True, timeout=5)
        if response_image.status_code == 200:
            with open(join(curr_path, remove_extension(file_filtered)+"-thumb.jpg"), 'wb') as f:
                copyfileobj(response_image.raw, f)
            print("SUCCESS", remove_extension(file_filtered))
    else:
        print("Cannot find", remove_extension(file_filtered))
